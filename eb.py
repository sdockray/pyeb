# -*- coding: utf-8 -*-

# Import the SDK
import boto3
from subprocess import call, check_output
from pprint import pprint


class EBClient(object):
    def __init__(self, region, profile=None):
        # Use a profile
        if profile:
            boto3.setup_default_session(profile_name=profile)
        # create a client
        self.client = boto3.client('elasticbeanstalk', region_name=region)
        self.ec2_client = boto3.client('ec2', region_name=region)
        self.rds_client = boto3.client('rds', region_name=region)
        self.s3_client = boto3.client('s3', region_name=region)
        self.ddb_client = boto3.client('dynamodb', region_name=region)

    def get_environments(self):
        environments = {}
        r = self.client.describe_environments()
        for environment in r['Environments']:
            environments[environment['EnvironmentName']] = environment
        return environments

    def get_environment(self, name):
        environments = self.get_environments()
        return environments[name] if name in environments else None

    def get_environment_instance(self, name):
        r = self.client.describe_environment_resources(EnvironmentName=name)
        return r['EnvironmentResources']['Instances'][0]['Id']

    def get_application_versions(self):
        r = self.client.describe_application_versions()
        return [v['VersionLabel'] for v in r['ApplicationVersions']]

    def get_events(self, name):
        events = self.client.describe_events(EnvironmentName=name)
        return events['Events']

    def get_environment_ips(self, name):
        environment = self.get_environment(name)
        i = self.get_environment_instance(name)
        r = self.ec2_client.describe_instances(InstanceIds=[i,])
        ips = [i['PublicIpAddress'] for i in r['Reservations'][0]['Instances']]
        return ips

    def get_environment_ip(self, name):
        return self.get_environment_ips(name)[0]

    def ssh(self, name, user, key, command=None, interactive=False):
        ips = self.get_environment_ips(name)
        if ips:
            if command and not interactive:
                return check_output(["ssh", "-i", key, user+"@"+ips[0], "{0}".format(command)], universal_newlines=True).strip()
            elif command and interactive:
                call(["ssh", "-i", key, user+"@"+ips[0], "{0}".format(command)])
            else:
                call(["ssh", "-i", key, user+"@"+ips[0]])
        else:
            print('Sorry, I could\'t get an IP for', name)

    def scp(self, name, user, key, local, remote, do_put):
        ips = self.get_environment_ips(name)
        if ips:
            if do_put:
                call(["scp", "-i", key, local, user+"@"+ips[0]+":"+remote])
            else:
                call(["scp", "-i", key, user+"@"+ips[0]+":"+remote, local])
        else:
            print('Sorry, I could\'t get an IP for', name)

    def ssh_docker(self, name, user, key):
        docker_name = self.ssh(name, user, key, 'sudo docker ps --format "{{.Names}}"')
        print("Docker container name: ", docker_name)
        self.ssh(name, user, key, command="sudo docker exec -i {0} /bin/bash".format(docker_name), interactive=True)

    def getenv(self, name, app_name):
        settings = self.client.describe_configuration_settings(EnvironmentName=name, ApplicationName=app_name)
        option_settings = settings['ConfigurationSettings'][0]['OptionSettings']
        vars = []
        for setting in option_settings:
            if setting['Namespace'] == "aws:elasticbeanstalk:application:environment":
                vars.append(setting)
        return vars

    def setenv(self, name, vars):
        option_settings = []
        for var in vars:
            option_settings.append({
                "Namespace": "aws:elasticbeanstalk:application:environment",
                "OptionName": var['name'],
                "Value": var['value']})
        self.client.update_environment(EnvironmentName=name, OptionSettings=option_settings)

    def deploy(self, name, version):
        self.client.update_environment(EnvironmentName=name, VersionLabel=version)

    def abort(self, name):
        self.client.abort_environment_update(EnvironmentName=name)

    # RDS stuff

    def list_database_instances(self):
        instances = self.rds_client.describe_db_instances()
        return [i for i in instances['DBInstances']]

    def list_database_snapshots(self, DBInstanceIdentifier=None):
        snapshots = self.rds_client.describe_db_snapshots(DBInstanceIdentifier=DBInstanceIdentifier)
        return [s for s in snapshots['DBSnapshots']]

    def create_database_snapshot(self, snapshot, instance):
        return self.rds_client.create_db_snapshot(DBSnapshotIdentifier=snapshot, DBInstanceIdentifier=instance)

    def rename_database_instance(self, instance):
        new_instance = instance+'-temp'
        response = self.rds_client.modify_db_instance(DBInstanceIdentifier=instance, NewDBInstanceIdentifier=new_instance)
        pprint(response)
        return new_instance

    def restore_database_snapshot(self, snapshot, instance):
        self.rename_database_instance(instance)
        return self.rds_client.restore_db_instance_from_db_snapshot(DBSnapshotIdentifier=snapshot, DBInstanceIdentifier=instance)

    # S3 stuff

    def list_buckets(self, filter):
        buckets = self.s3_client.list_buckets()
        if filter:
            return [b for b in buckets['Buckets'] if filter in b['Name']]
        else:
            return [b for b in buckets['Buckets']]

    def list_bucket(self, bucket):
        objects = self.s3_client.list_objects_v2(Bucket=bucket)
        return objects['Contents']

    def put_file_in_bucket(self, bucket, source, dest):
        return self.s3_client.upload_file(source, bucket, dest)

    def get_presigned_url(self, bucket, key):
        return self.s3_client.generate_presigned_url('get_object',
            Params={
                'Bucket': bucket,
                'Key': key,
            },
            ExpiresIn=3600)

    # DynamoDB stuff

    def list_ddb_tables(self):
        tables = self.ddb_client.list_tables()
        return tables['TableNames']


    def get_ddb_table(self, table):
        return self.ddb_client.describe_table(TableName=table)


    def empty_ddb_table(self, table):
        info = self.get_ddb_table(table)
        response = self.ddb_client.delete_table(TableName=table)
        schema = info['Table']
        if 'ProvisionedThroughput' in schema and 'NumberOfDecreasesToday' in schema['ProvisionedThroughput']:
            del schema['ProvisionedThroughput']['NumberOfDecreasesToday']
        keys = ['AttributeDefinitions', 'TableName', 'KeySchema', 'LocalSecondaryIndexes', 'GlobalSecondaryIndexes', 'BillingMode', 'ProvisionedThroughput', 'StreamSpecification', 'SSESpecification']
        new_schema = {k: schema[k] for k in keys if k in schema.keys()}
        print('Deleting table:', table)
        waiter = self.ddb_client.get_waiter('table_not_exists')
        waiter.wait(TableName=table)
        return self.ddb_client.create_table(**new_schema)
