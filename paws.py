#!/Users/seandockray/.virtualenvs/aws/bin/python
# -*- coding: utf-8 -*-
"""paws CLI
Usage:
    paws.py <profile_name>
    -r <region>, default "ap-southeast-2"
    -u <ssh user>, default "ec2-user"
"""

import time
import sys
import datetime
import os
import fnmatch
from pprint import pprint

from eb import EBClient

# sys.path.insert(0, os.path.abspath('..'))

from clint.arguments import Args
from clint.textui import prompt, puts, colored, validators

# Assuming a default region based on prior projects
# Use -r to set a different region
DEFAULT_REGION = 'ap-southeast-2'
# It would be nice to not have this hard-coded, but I also don't want to have to type in
# the user every time we want to scp, ssh, or run remote command, so here it is for now.
# Use -u to set a different user
DEFAULT_USER = 'ec2-user'


args = Args()
params = Args().grouped


# Getting IP Address
def get_ip(client, **kwargs):
    ip = client.get_environment_ip(kwargs['EnvironmentName'])
    puts("The IP Address of {0} is: {1}".format(colored.blue(kwargs['EnvironmentName']), colored.blue(ip)))


# Getting Amazon URL
def get_url(client, **kwargs):
    environment = client.get_environment(kwargs['EnvironmentName'])
    puts("The URL for {0} is: {1}".format(
        colored.blue(kwargs['EnvironmentName']),
        colored.blue(environment['EndpointURL'])))


# Getting more information about an environment
def get_info(client, **kwargs):
    environment = client.get_environment(kwargs['EnvironmentName'])
    pprint(environment)
    # start_action(client, **kwargs)


# Getting Status of environment
def get_status(client, **kwargs):
    environment = client.get_environment(kwargs['EnvironmentName'])
    # pprint(environment)
    puts("The status of {0} is: {1} ({2})".format(
        colored.blue(kwargs['EnvironmentName']),
        colored.blue(environment['Status']),
        colored.blue(environment['Health'])))


# Polls an environment
def poll(client, **kwargs):
    starttime=time.time()
    while True:
        get_status(client, **kwargs)
        time.sleep(10.0 - ((time.time() - starttime) % 10.0))


# SSH to server
def ssh(client, **kwargs):
    pem = choose_pem()
    client.ssh(kwargs['EnvironmentName'], kwargs['ssh_user'], pem)


# SCP to server
def scp(client, **kwargs):
    options = [{'selector': 'p', 'prompt': 'Put', 'return': True},
               {'selector': 'g', 'prompt': 'Get', 'return': False}]
    do_put = prompt.options(colored.green("Are you getting a file or putting one?"), options)
    if do_put:
        local = prompt.query("File to put: ")
        remote = prompt.query("Remote filepath: ")
    else:
        remote = prompt.query("File to get: ")
        local = prompt.query("Local filepath: ")
    pem = choose_pem()
    client.scp(kwargs['EnvironmentName'], kwargs['ssh_user'], pem, local, remote, do_put)


# Deploy a version to an environment
def deploy(client, **kwargs):
    version = choose_version(client)
    puts("Deploying {0} to {1}".format(colored.blue(version), colored.blue(kwargs['EnvironmentName'])))
    if prompt.query("Are you sure? (y to confirm)") == 'y':
        puts(colored.green('Deploying!'))
        client.deploy(kwargs['EnvironmentName'], version)
        poll(client, **kwargs)
    else:
        puts(colored.red("Deployment canceled!"))


# Deploy a version to an environment
def abort(client, **kwargs):
    client.abort(kwargs['EnvironmentName'])


# Get environment variables
def get_variables(client, **kwargs):
    environment = client.get_environment(kwargs['EnvironmentName'])
    vars = client.getenv(kwargs['EnvironmentName'], environment['ApplicationName'])
    for var in vars:
        puts("{0}: {1}".format(colored.blue(var['OptionName']), colored.green(var['Value'])))


# Sets an environment variable
def set_variables(client, **kwargs):
    vars = []
    more = True
    while more:
        new_var = {'name': None, 'value': None}
        new_var['name'] = prompt.query("Environment variable name: ")
        new_var['value'] = prompt.query("Environment variable value: ")
        if new_var['name'] and new_var['value']:
            vars.append(new_var)
        more = (prompt.query("Add another? (y to set another variable)") == "y")
    if vars:
        puts(colored.green('Setting environment variables:'))
        for var in vars:
            puts("{0}: {1}".format(colored.blue(var['name']), colored.blue(var['value'])))
        client.setenv(kwargs['EnvironmentName'], vars)


# Polls an environment
def poll_events(client, **kwargs):
    starttime=time.time()
    events = []
    while True:
        new_events = [e for e in client.get_events(kwargs['EnvironmentName']) if e not in events]
        print_events(client, new_events, **kwargs)
        events.extend(new_events)
        time.sleep(10.0 - ((time.time() - starttime) % 10.0))


# Gets recent events
def print_events(client, events, **kwargs):
    for event in events[::-1]:
        puts("{0}: {1}".format(event['EventDate'], colored.green(event['Message'])))


# Runs a remote command
def run(client, **kwargs):
    pem = choose_pem()
    command = prompt.query("What is the remote command you want to run? ")
    output = client.ssh(kwargs['EnvironmentName'], kwargs['ssh_user'], pem, command)
    puts("{0}".format(colored.blue(output)))


# Runs a remote command
def ssh_docker(client, **kwargs):
    pem = choose_pem()
    puts("Attempting to open a shell session in a Docker container in {0}".format(colored.blue(kwargs['EnvironmentName'])))
    client.ssh_docker(kwargs['EnvironmentName'], kwargs['ssh_user'], pem)


# List database snapshots
def list_snapshots(client, **kwargs):
    instance = choose_db_instance(client)
    snapshots = client.list_database_snapshots(DBInstanceIdentifier=instance)
    for s in snapshots:
        puts("{0}: {1}".format(colored.blue(s['DBInstanceIdentifier']), colored.green(s['DBSnapshotIdentifier'])))


# List database snapshots
def snapshot(client, **kwargs):
    instance = choose_db_instance(client)
    prefix = prompt.query("Prefix string: ")
    name = datetime.datetime.now().strftime("{0}-%Y-%m-%d%H%M%S".format(prefix))
    puts("Attempting RDS Snapshot {0} to {1}".format(colored.blue(instance), colored.blue(name)))
    response = client.create_database_snapshot(name, instance)
    pprint(response)


# List database snapshots
def restore_snapshot(client, **kwargs):
    instance = choose_db_instance(client)
    snapshot = prompt.query("Snapshopt name: ")
    puts("Attempting to restore RDS Snapshot {0} to {1}".format(colored.blue(snapshot), colored.blue(instance)))
    response = client.restore_database_snapshot(snapshot, instance)
    pprint(response)


# List S3 buckets
def list_bucket(client, **kwargs):
    bucket = choose_bucket(client)
    objects = client.list_bucket(bucket)
    for o in objects:
        puts("{0}".format(o['Key']))


# List S3 buckets
def put_in_bucket(client, **kwargs):
    bucket = choose_bucket(client)
    orig = prompt.query("Path to file you want to upload:")
    dest_default = orig.rsplit('/', 1)[-1]
    dest = prompt.query("Destination object on S3:", default=dest_default)
    client.put_file_in_bucket(bucket, orig, dest)


# Get a download URL of bucket
def url_in_bucket(client, **kwargs):
    bucket = choose_bucket(client)
    objects = client.list_bucket(bucket)
    for o in objects:
        puts("{0}".format(o['Key']))
    file = prompt.query("Which file do you want a URL for?")
    puts("{0}".format(colored.blue(client.get_presigned_url(bucket, file))))


# Make a new bucket and a hello world website in it on S3
def website():
    pass


# Prints a count of the records in a DynamoDb table
def ddb_count_table(client, **kwargs):
    table = choose_table(client)
    info = client.get_ddb_table(table)
    pprint(info)


# Empties a DynamoDb table
def ddb_empty_table(client, **kwargs):
    table = choose_table(client)
    response = client.empty_ddb_table(table)
    pprint(response)


# List actions
def list_actions():
    return[
        { 'selector': 'i', 'prompt': 'Get info', 'return': get_info},
        { 'selector': 'ip', 'prompt': 'Get IP Address', 'return': get_ip},
        { 'selector': 1, 'prompt': 'Get Status', 'return': poll},
        { 'selector': 2, 'prompt': 'Deploy', 'return': deploy},
        { 'selector': 3, 'prompt': 'Abort Deployment', 'return': abort},
        { 'selector': 4, 'prompt': 'Get Environment Variables', 'return': get_variables},
        { 'selector': 5, 'prompt': 'Set Environment Variables', 'return': set_variables},
        { 'selector': 'e', 'prompt': 'Get recent events', 'return': poll_events},
        { 'selector': 'ssh', 'prompt': 'SSH', 'return': ssh},
        { 'selector': 'scp', 'prompt': 'SCP', 'return': scp},
        { 'selector': 'cmd', 'prompt': 'Run a command', 'return': run},
        { 'selector': 'd', 'prompt': 'SSH into a Docker container', 'return': ssh_docker},
        { 'selector': 'l', 'prompt': 'List snapshots', 'return': list_snapshots},
        { 'selector': 's', 'prompt': 'Take database snapshot', 'return': snapshot},
        #{ 'selector': 'r', 'prompt': 'Restore database snapshot', 'return': restore_snapshot},
        { 'selector': 'lb', 'prompt': 'List objects in a bucket', 'return': list_bucket},
        { 'selector': 'pb', 'prompt': 'Puts a file in a bucket', 'return': put_in_bucket},
        { 'selector': 'url', 'prompt': 'URL of something in bucket', 'return': url_in_bucket},
        #{ 'selector': 'website', 'prompt': 'Create a quick S3 website', 'return': website}
        { 'selector': 'ddb', 'prompt': 'Info about a DynamoDB table', 'return': ddb_count_table},
        { 'selector': 'ddb-empty', 'prompt': 'Empty a DynamoDB table', 'return': ddb_empty_table},
    ]


# Show prompt to choose a DynamoDB table
def choose_table(client):
    return prompt.options(colored.green("Choose a table:"), list_tables(client))


# Returns a prompt friendly list of DynamoDB tables
def list_tables(client):
    tables = client.list_ddb_tables()
    options = []
    for i in tables:
        options.append({
            'selector': len(options) + 1,
            'prompt': i,
            'return': i
        })
    return options


# Show prompt to choose a bucket
def choose_bucket(client):
    return prompt.options(colored.green("Choose a bucket:"), list_buckets(client))


# Returns a prompt friendsly list of S3 buckets
def list_buckets(client):
    filter = prompt.query("Filter:")
    buckets = client.list_buckets(filter=filter)
    options = []
    for i in buckets:
        options.append({
            'selector': len(options) + 1,
            'prompt': i['Name'],
            'return': i['Name']
        })
    return options


# Show prompt to choose an action
def choose_action():
    return prompt.options(colored.green("What do you want?"), list_actions())


# returns a prompt-friendly list of all the database instances
def list_db_instances(client):
    instances = client.list_database_instances()
    options = []
    for i in instances:
        options.append({
            'selector': len(options) + 1,
            'prompt': i['DBInstanceIdentifier'],
            'return': i['DBInstanceIdentifier']
        })
    return options


# Show prompt to choose an instance
def choose_db_instance(client):
    return prompt.options(colored.green("Choose an instance:"), list_db_instances(client))


# returns a prompt-friendly list of all the environments
def list_environments(client):
    def filter_prompt(e):
        if e == 'prov-website-eb-210119':
            return e + colored.red(' --> jan 21 copy of prod')
        elif e == 'prov-beta-website-eb':
            return e + colored.red(' --> prov.vic.gov.au')
        elif e == 'prov-prod-website-eb':
            return e + colored.red(' --> beta.prov.vic.gov.au')
        else:
            return e
    options = []
    environments = client.get_environments()
    for e in environments:
        options.append({
            'selector': len(options) + 1,
            'prompt': filter_prompt(e),
            'return': e
        })
    return options


# Show prompt to choose an environment
def choose_environment(client):
    environment_choices = list_environments(client)
    if environment_choices:
        return prompt.options(colored.green("Choose an environment:"), environment_choices)
    else:
        return None


# returns a prompt friendly list of pem files
def list_pems():
    options =  []
    listOfFiles = os.listdir(os.path.join(os.path.expanduser('~'), '.ssh'))
    pattern = "*.pem"
    for entry in listOfFiles:
        if fnmatch.fnmatch(entry, pattern):
            options.append({
                'selector': len(options) + 1,
                'prompt': entry,
                'return': os.path.join(os.path.join(os.path.expanduser('~'), '.ssh', entry))
            })
    return options


# Show prompt to choose an environment
def choose_pem():
    return prompt.options(colored.green("Choose a pem file (for an SSH key):"), list_pems())


# Options for application versions, including a filter
def list_versions(client):
    options = []
    filter = prompt.query("CircleCI build number (or other string to filter by):")
    versions = client.get_application_versions()
    filtered_versions = [v for v in versions if filter in v]
    for v in filtered_versions:
        options.append({
            'selector': len(options) + 1,
            'prompt': v,
            'return': v
        })
    return options

# Show prompt to choose a version
def choose_version(client):
    options = list_versions(client)
    if options:
        return prompt.options(colored.green("Choose an application version to deploy:"), options)
    else:
        puts(colored.red("No versions found!"))
        return choose_version(client)

# Sets an action in motion
def start_action(client, **kwargs):
    try:
        action_method = choose_action()
        action_method(client, EnvironmentName=environment_name, ssh_user=user)
    except KeyboardInterrupt:
        puts(colored.green("\nGoodbye!"))
        sys.exit()


if __name__ == '__main__':
    profile = None
    if not args[0] or args[0] in params:
        puts(colored.blue("You are not using a profile. If you have multiple profiles listed in ~/.aws/credentials, this won't work. You'll need to provide a profile name."))
    else:
        puts(colored.blue("Using profile: {0}".format(args[0])))
        profile = args[0]
    region = params['-r'][0] if '-r' in params else DEFAULT_REGION
    user = params['-u'][0] if '-u' in params else DEFAULT_USER
    puts(colored.blue("Using region: {0}".format(region)))
    #
    client = EBClient(region, profile=profile)
    try:
        environment_name = choose_environment(client)
    except KeyboardInterrupt:
        puts(colored.green("\nGoodbye!"))
        sys.exit()
    start_action(client, EnvironmentName=environment_name, ssh_user=user)
