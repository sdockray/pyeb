# pyeb

A Python utility for interacting with Amazon Elasticbeanstalk

Note: I'm writing this as a replacement for the directory of PRoV scripts

## Installation from source

```
git clone git@bitbucket.org:sdockray/pyeb.git
cd pyeb
mkvirtualenv aws -p python3
workon aws
pip install -r requirements.txt
```

To run anywhere, change interpreter (in `paws.py`) to match yours:
```
#!/Users/seandockray/.virtualenvs/aws/bin/python
```
and add to `PATH` and make `paws.py` executable

## Usage

* You should have AWS credentials in `~/.aws/credentials` (profiles are ok!)
* For SSH, you should have made a user in AWS and gotten .pem keys (stored in your `~/.ssh`)

```
paws <profile name> -u <ssh user> -r <region>
```


